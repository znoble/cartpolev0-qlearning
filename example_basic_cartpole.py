#!/usr/bin/env python3

import gym
from random import choice
import math
'''
    cartpole-v0:  200 max moves
    cartpole-v1:  500 max moves
    state = [position, velocity, angle, angle_velocity]
    end goal: keep values in range
    keep track of how many times we are in each state
    partitions example:
        positions:                     -3 <-> 0 <-> 3
        velocity:                      -4 <-> 0 <-> 4
        angle:             -0.5 <-> -0.25 <-> 0 <-> 0.25 <-> 0.5
        angle velocity:    -4   <->  -1  <->  0 <->  1   <->  4

    intialize qtable to 0 
          qtable = Dictionary: key=state, value=[est of going left, est of going right]
          explore vs exploit:
               explore: look for new values
               exploit: use what we know already
          qtable: indexed by state, possible action

          qtable is updated by bellmans equation
          V(s_t) == max E(R_t+1 + gamma*V(S_t+1))
        NewValue                        Oldvalue
             the val of the current state is the maximum value of the expected val
             of the reward of the next state plust the discount rate times the
             value of the next state.

    alpha    = learning rate   (should decrease as the game goes on) 
    epsilon  = exploration vs exploitation rate (below is exploration, above is explotation)
    gamma    = discount rate
    
    Update V(s_t) = alpha * NewValue + (1 - alpha) * OldValue
    
    try to get it with 500 rounds of learning
'''
def randomchoice():
    return choice([0,1])

def physics(th, sig):
    if th < 0 and sig < 0:
        return 0
    elif th > 0 and sig > 0:
        return 1
    else:
        return choice([0,1])

if __name__ == "__main__":
    env = gym.make('CartPole-v0')

    niter = 0
    totalmoves = 0
    while True:
        x1 = env.reset()              # game setup
        x2 = 1.0
        steps = 0
        while True:                   # game begins
            steps += 1
            env.render()              # draw cart and pole
            th = x1[2]
            sig = x1[3]
            y = physics(th, sig) 
            x1, x2, done, x3 = env.step(y)                # make move and get feedback
            print(x1,x2, done, x3)
            if done:                                      # gameover, score == num moves
                totalmoves += steps
                niter += 1
                print(f"{steps:3d} {totalmoves/niter:9.3f}")
                print(x1)
                input()
                break
    env.close()
