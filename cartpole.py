#!/usr/bin/env python3

import sys
import gym
import numpy as np
from random import choice, random, shuffle
import math
import matplotlib.pyplot as plt

SOLVED = 195.0

class Agent:

    def __init__(self, ntrain=1000, ntest=100, render=False):
        self.env = gym.make('CartPole-v0')        
        self.render = render
        self.alpha    = 1.0     # learning rate
        self.minalpha = 0.05
        self.decay    = 550.0
        self.gamma    = 0.90    # discount rate
        self.epsilon  = 0.90    # exploration rate
        self.minepsilon = 0.05

        self.ntrain = ntrain
        self.trainIters = []
        self.ntest = ntest
        self.testIters = []

        self.buckets = [3,3,6,6]
        self.envlow = self.env.observation_space.low
        self.envhigh = self.env.observation_space.high
        self.bucket_ranges = [[-2.75,2.75], [-4.50,4.50], [-0.40, 0.40], [-4.00,4.00]]    
        self.bucket_endpoints = {}
        self.createbuckets()

        self.qtable = {(a,b,c,d): [0,0] for a in range(self.buckets[0]) for b in range(self.buckets[1]) for c in range(self.buckets[2]) for d in range(self.buckets[3])}
        self.visits = {(a,b,c,d): [0,0] for a in range(self.buckets[0]) for b in range(self.buckets[1]) for c in range(self.buckets[2]) for d in range(self.buckets[3])}


    def createbuckets(self):
        for i in range(len(self.buckets)):
            ranges = []
            r = self.bucket_ranges[i]
            n = self.buckets[i]
            totallen = sum([abs(x) for x in r])
            bucketlen = totallen / n
            p1 = r[0]
            p2 = p1 + bucketlen
            ranges.append((p1, p2))
            for j in range(1, n):
                p1 = p2
                p2 += bucketlen
                ranges.append((p1, p2))
            self.bucket_endpoints[i] = ranges                    

    def discretizestate(self, state):
        disc_state = []
        for i in range(len(state)):
            bucket_ranges = self.bucket_endpoints[i]
            for j in range(len(bucket_ranges)):
                if bucket_ranges[j][0] <= state[i] < bucket_ranges[j][1]:
                    disc_state.append(j)
                    break
        if len(disc_state) < 4:
            print("State value out of bounds.")
            print(disc_state, state)
            sys.exit()
        return tuple(disc_state)

    def train(self):
        for i in range(self.ntrain):
            step = 0
            state = self.env.reset()
            done = False
            while not done:
                step += 1
                oldstate = self.discretizestate(state)
                move = self.getmove(oldstate)
                state, reward, done, _ = self.env.step(move)
                newstate = self.discretizestate(state)
                if done and step==200:
                    reward = 100
                elif done and step < 200:
                    reward = -100 
                self.feedback(newstate, oldstate, move, reward, i)
            self.trainIters.append(step)
       
    def run(self):
        self.epsilon = 0.0
        for i in range(self.ntest):
            step = 0
            state = self.env.reset()
            done = False
            while not done:
                step += 1
                if self.render:
                    self.env.render()
                oldstate = self.discretizestate(state)
                move = self.getmove(oldstate)
                state, reward, done, _ = self.env.step(move)
            self.testIters.append(step)
            if self.render:
                print(step)
                input()
        avg_step = sum(self.testIters) / len(self.testIters)
        if avg_step > SOLVED:
            print(f"CartPole-v0 solved with average return {avg_step} in {len(self.testIters)} iterations.")
        else:
            print(f"CartPole-v0 not solved yet...average return {avg_step} in {len(self.testIters)} iterations.")
        numMax = sum([1 for x in self.testIters if x == 200])
        print(f"{numMax} completed. ratio {numMax/len(self.testIters)}")

    def getmove(self, state):
        crit = random()
        if crit < self.epsilon:          #exploration route
            li = list(range(2))
            shuffle(li) 
            direc = min(li, key=lambda x: self.visits[state][x])    # go the least visisted direction
        else:                            # exploitation route
            direc = max(range(2), key=lambda x: self.qtable[state][x])    # go the best valued direction
        return direc

    def feedback(self, newstate, oldstate, move, reward, trialnum):
        # this method updates qtable based on bellman's equation
        self.visits[newstate][move] += 1
        chn = self.alpha * (reward + self.gamma * self.qtable[newstate][move])
        self.qtable[oldstate][move] = chn + (1-self.alpha)*self.qtable[oldstate][move]
        self.alpha = max(self.minalpha, min(1.0, math.exp(-trialnum/self.decay)))
        self.epsilon = max(self.minepsilon, min(.99, math.exp(-trialnum/self.decay)))

    def close(self):
        self.env.close()    
       
    def dumpVisits(self):
        print("\n Visits Table ")
        numVisited = sum([1 for x in self.visits if sum(self.visits[x]) > 0])
        print(numVisited)
        sortedVisits = sorted(self.visits, key=lambda x:self.visits[x], reverse=True)
        for state in sortedVisits:
            if sum(self.visits[state]) == 0:
                continue
            print("{0:5s} {1:10s}".format(str(state), str(sum(self.visits[state]))))
        print()


    def showPlot(self):
        data = np.array(self.trainIters)
        print(data.mean(), data.std())
        plt.scatter(range(len(data)), data)
        plt.show() 


if __name__ == "__main__":

    if len(sys.argv) > 1: 
        render = True
    else:
        render = False
    agent = Agent(render=render)
    agent.train()
    agent.showPlot()
    agent.run()
    #agent.dumpVisits()
    agent.close()        
